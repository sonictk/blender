#ifndef BLI_DLL_H
#define BLI_DLL_H

#define BLI_UNSUPPORTED_PLATFORM "Unsupported platform for compilation."


#ifdef _WIN32

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
typedef HMODULE BLI_DLLHandle;
typedef HANDLE BLI_FileHandle;
typedef FARPROC BLI_FuncPtr;

#elif __linux__ || __APPLE__
#include <dlfcn.h>
#include <stdio.h>
typedef void * BLI_DLLHandle;
typedef FILE *BLI_FileHandle;
typedef void *BLI_FuncPtr;

#else
#error BLI_UNSUPPORTED_PLATFORM
#endif // Platform layer


BLI_DLLHandle BLI_load_dll_with_flags(const char *const filename, int flags);

BLI_DLLHandle BLI_load_dll(const char *const filename);


int BLI_unload_dll(BLI_DLLHandle handle);


BLI_FuncPtr BLI_load_sym_from_lib(BLI_DLLHandle handle, const char *const symbol);

#endif /* BLI_DLL_H */
