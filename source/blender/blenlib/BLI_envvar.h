#ifndef BLI_ENVVAR_H
#define BLI_ENVVAR_H

#ifdef _WIN32
#define ENV_VAR_MAX_LEN 32767
#define OS_PATH_SEP '\\'
#elif __linux__ || __APPLE__
#define ENV_VAR_MAX_LEN 131072
#define OS_PATH_SEP '/'
#endif // Platform layer

int BLI_get_environ_variable(const char *name, char *buf, const int size);


#endif /* BLI_ENVVAR_H */
