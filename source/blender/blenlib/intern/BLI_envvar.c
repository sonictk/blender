#include "BLI_envvar.h"


#ifdef _WIN32
#include <Windows.h>

int BLI_get_environ_variable(const char *name, char *buf, const int size)
{
	DWORD result = GetEnvironmentVariable((LPCTSTR)name, (LPTSTR)buf, (DWORD)size);
	return (int)result;
}

#elif __linux__ || __APPLE__
#include <stdlib.h>
#include <string.h>

int BLI_get_environ_variable(const char *name, char *buf, const int size)
{
	(void)(size);
	buf = getenv(name);
	if (buf == NULL) {
		return 0;
	}
	int result = strlen(buf);
	return result;
}
#endif // Platform layer
