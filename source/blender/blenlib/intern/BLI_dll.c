#include "BLI_dll.h"
#include <assert.h>

BLI_DLLHandle BLI_load_dll_with_flags(const char *const filename, int flags)
{
#ifdef _WIN32
	BLI_DLLHandle libH = LoadLibraryA((LPCSTR)filename);
#elif __linux__ || __APPLE__
	BLI_DLLHandle libH = dlopen(filename, flags);
#else
#error BLI_UNSUPPORTED_PLATFORM
#endif // Platform layer
	return libH;
}

BLI_DLLHandle BLI_load_dll(const char *const filename)
{
	assert(filename != NULL);
#ifdef _WIN32
	BLI_DLLHandle libH = BLI_load_dll_with_flags(filename, 0);
#elif __linux__ || __APPLE__
	BLI_DLLHandle libH = BLI_load_dll_with_flags(filename, RTLD_LAZY);
#else
#error BLI_UNSUPPORTED_PLATFORM
#endif // Platform layer
	return libH;
}


int BLI_unload_dll(BLI_DLLHandle handle)
{
	assert(handle != NULL);
#ifdef _WIN32
	BOOL bResult = FreeLibrary(handle);
	if (bResult != 0) {
		return 0;
	}
#elif __linux__ || __APPLE__
	int result = dlclose(handle);
	if (result == 0) {
		return 0;
	}
#else
#error BLI_UNSUPPORTED_PLATFORM
#endif // Platform layer
	return 1;
}


BLI_FuncPtr BLI_load_sym_from_lib(BLI_DLLHandle handle, const char *const symbol)
{
	assert(handle != NULL);
	assert(symbol != NULL);
	BLI_FuncPtr result;
#ifdef _WIN32
	result = GetProcAddress(handle, (LPCSTR)symbol);
#elif __linux__ || __APPLE__
	result = dlsym(handle, symbol);
	// NOTE: (sonictk) Following recommendation of Linux manual; clear the
	// old error code, call ``dlsym()`` again, and then call ``dlerror()``
	// again since the symbol returned could actually be ``NULL``.
	if (result == NULL) {
		dlerror();
		dlsym(handle, symbol);
		char *errMsg = dlerror();
		if (errMsg == NULL) {
			return result;
		}
		return NULL;
	}
#else
#error BLI_UNSUPPORTED_PLATFORM
#endif //
	return result;
}
