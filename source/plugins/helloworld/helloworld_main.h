#ifndef HELLOWORLD_MAIN_H
#define HELLOWORLD_MAIN_H

#ifdef _WIN32
#define export_sym __declspec(dllexport)
#define import_sym __declspec(dllimport)

#elif __linux__ || __APPLE__
#define export_sym __attribute__ ((visibility ("default")))
#define import_sym export_sym
#endif // Platform layer


extern export_sym void hello_world();


#endif /* HELLOWORLD_MAIN_H */
