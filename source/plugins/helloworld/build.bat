@echo off
REM    This is the Windows build script for the Blender Hello World plugin.
REM    usage: build.bat [debug|release|relwithdebinfo|clean] [2012|2015|2017|2019]

setlocal

echo Build script started executing at %time% ...
REM Process command line arguments. Default is to build in release configuration.
set BuildType=%1
if "%BuildType%"=="" (set BuildType=release)

set ProjectName=helloworld

set MSVCVersion=%2
if "%MSVCVersion%"=="" (set MSVCVersion=2019)

echo Building %ProjectName% in %BuildType% configuration using MSVC %MSVCVersion%...

REM    Set up the Visual Studio environment variables for calling the MSVC compiler;
REM    we do this after the call to pushd so that the top directory on the stack
REM    is saved correctly; the check for DevEnvDir is to make sure the vcvarsall.bat
REM    is only called once per-session (since repeated invocations will screw up
REM    the environment)

if defined DevEnvDir goto build_setup

if %MSVCVersion%==2015 (
   call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
   goto build_setup

)

if %MSVCVersion%==2017 (
   call "%vs2017installdir%\VC\Auxiliary\Build\vcvarsall.bat" x64
   goto build_setup
)

if %MSVCVersion%==2019 (
   call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
   goto build_setup
)

echo Invalid MSVC compiler version specified.
goto error

:build_setup


REM    Make a build directory to store artifacts; remember, %~dp0 is just a special
REM    FOR variable reference in Windows that specifies the current directory the
REM    batch script is being run in
set BuildDir=%~dp0msbuild

if "%BuildType%"=="clean" (
    REM This allows execution of expressions at execution time instead of parse time, for user input
    setlocal EnableDelayedExpansion
    echo Cleaning build from directory: %BuildDir%. Files will be deleted^^!
    echo Continue ^(Y/N^)^?
    set /p ConfirmCleanBuild=
    if "!ConfirmCleanBuild!"=="Y" (
        echo Removing files in %BuildDir%...
        del /s /q %BuildDir%\*.*
    )
    goto end
)


echo Building in directory: %BuildDir%

if not exist %BuildDir% mkdir %BuildDir%

if %errorlevel% neq 0 goto error

pushd %BuildDir%


REM    Set up globals
set PluginEntryPoint=%~dp0%ProjectName%_main.c
set PluginDeployPath=%BLENDER_PLUGINS_PATH%

REM    We pipe errors to null, since we don't care if it fails
del *.pdb > NUL 2> NUL


REM    Setup all the compiler flags
set CommonCompilerFlags=/c /W4 /WX /nologo /EHsc

set CommonCompilerFlagsDebug=/Zi /Od %CommonCompilerFlags%
set CommonCompilerFlagsRelease=/O2 %CommonCompilerFlags%

set PluginCompilerFlagsDebug=%CommonCompilerFlagsDebug% %PluginEntryPoint% /Fo"%BuildDir%\%ProjectName%.obj"
set PluginCompilerFlagsRelease=%CommonCompilerFlagsRelease% %PluginEntryPoint% /Fo"%BuildDir%\%ProjectName%.obj"

REM    Setup all the linker flags
set CommonLinkerFlags=/nologo /incremental:no /machine:x64 /dll

REM    Now add the OS libraries to link against
set CommonLinkerFlags=%CommonLinkerFlags% /pdb:"%BuildDir%\%ProjectName%.pdb" /implib:"%BuildDir%\%ProjectName%.lib"

set CommonLinkerFlagsDebug=/debug /opt:noref %CommonLinkerFlags%
set CommonLinkerFlagsRelease=/opt:ref %CommonLinkerFlags%

set PluginExtension=dll

set PluginLinkerFlagsCommon=/out:"%BuildDir%\%ProjectName%.%PluginExtension%"
set PluginLinkerFlagsRelease=%PluginLinkerFlagsCommon% %CommonLinkerFlagsRelease% "%BuildDir%\%ProjectName%.obj"
set PluginLinkerFlagsDebug=%PluginLinkerFlagsCommon% %CommonLinkerFlagsDebug% "%BuildDir%\%ProjectName%.obj"


if "%BuildType%"=="debug" (
    echo Building in debug mode...

    set PluginCompilerFlags=%PluginCompilerFlagsDebug%
    set PluginLinkerFlags=%PluginLinkerFlagsDebug%

) else (
    echo Building in release mode...

    set PluginCompilerFlags=%PluginCompilerFlagsRelease%
    set PluginLinkerFlags=%PluginLinkerFlagsRelease%
)


REM Now build the  plugin
echo Compiling plugin (command follows)...
echo cl %PluginCompilerFlags%
cl %PluginCompilerFlags%
if %errorlevel% neq 0 goto error


:link
echo Linking (command follows)...
echo link %PluginLinkerFlags%
link %PluginLinkerFlags%
if %errorlevel% neq 0 goto error


echo Copying file(s) to deployment location...
echo copy /Y "%BuildDir%\%ProjectName%.%PluginExtension%" "%BLENDER_PLUGINS_PATH%"
copy /Y "%BuildDir%\%ProjectName%.%PluginExtension%" "%BLENDER_PLUGINS_PATH%"
if %errorlevel% neq 0 goto error
if %errorlevel% == 0 goto success


:error
echo ***************************************
echo *      !!! An error occurred!!!       *
echo ***************************************
goto end


:success
echo ***************************************
echo *    Build completed successfully!    *
echo ***************************************
goto end


:end
echo Build script finished execution at %time%.
popd
exit /b %errorlevel%
