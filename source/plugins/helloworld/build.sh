#!/usr/bin/env bash

# This is the Linux build script for the Blender Hello World plugin.
# usage: build.sh [debug|release|relwithdebinfo|clean]

StartTime=`date +%T`;
echo "Build script started executing at ${StartTime}...";

# Process command line arguments
BuildType=$1;

if [ "${BuildType}" == "" ]; then
    BuildType="release";
fi;

ProjectName="helloworld";

Compiler=$2;

if [ "${Compiler}" == "" ]; then
    Compiler="gcc";
fi;

GCCPath="gcc";

echo "Building ${ProjectName} in ${BuildType} configuration using ${Compiler}...";

# Define colours to be used for terminal output messages
RED='\033[0;31m';
GREEN='\033[0;32m';
NC='\033[0m'; # No Color

# Create a build directory to store artifacts
BuildDir="${PWD}/linuxbuild";

# If cleaning builds, just delete build artifacts and exit immediately
if [ "${BuildType}" == "clean" ]; then
    echo "Cleaning build from directory: ${BuildDir}. Files will be deleted!";
    read -p "Continue? (Y/N)" ConfirmCleanBuild;
    if [ $ConfirmCleanBuild == [Yy] ]; then
       echo "Removing files in: ${BuildDir}...";
       rm -rf $BuildDir;
    fi;

    exit 0;
fi;

echo "Building in directory: ${BuildDir}";
if [ ! -d "${BuildDir}" ]; then
   mkdir -p "${BuildDir}";
fi;

PluginEntryPoint="${PWD}/${ProjectName}_main.c";
PluginDeployPath="${BLENDER_PLUGINS_PATH}";

# Setup C/C++ compilation settings
CommonCompilerFlags="-m64 -fPIC -fvisibility=hidden -fpermissive";
CommonCompilerFlags="${CommonCompilerFlags} -Wall -Wextra -Werror";
CompilerFlagsDebug="-ggdb -O0 ${CommonCompilerFlags} -D_DEBUG";
CompilerFlagsRelease="-O2 ${CommonCompilerFlags} -DNDEBUG";
CompilerFlagsRelWithDebInfo="-ggdb -O2 ${CommonCompilerFlags} -DNDEBUG";

CommonLinkerFlags="-lm,-lpthread";
LinkerFlagsDebug="${CommonLinkerFlags}";
LinkerFlagsRelease="${CommonLinkerFlags},-s";
LinkerFlagsRelWithDebInfo="${CommonLinkerFlags}";

LibraryLinkerFlagsCommon="-shared -fPIC";
LibraryLinkerFlagsDebug="${LibraryLinkerFlagsCommon} -O0";
LibraryLinkerFlagsRelease="${LibraryLinkerFlagsCommon}";
LibraryLinkerFlagsRelWithDebInfo="${LibraryLinkerFlagsCommon}";

if [ "${BuildType}" == "debug" ]
then
    BuildCommand="${Compiler} ${EntryPoint} ${CompilerFlagsDebug} -o ${OutBin} -Wl,${LinkerFlagsDebug}";
    BuildDLLCommand="${Compiler} ${DLLEntryPoint} ${CompilerFlagsDebug} -shared -o ${OutDLL} -Wl,${LibraryLinkerFlagsDebug}";
elif [ "${BuildType}" == "relwithdebinfo" ]
then
    BuildCommand="${Compiler} ${EntryPoint} ${CompilerFlagsRelWithDebInfo} -o ${OutBin} -Wl,${LinkerFlagsRelWithDebInfo}";
    BuildDLLCommand="${Compiler} ${DLLEntryPoint} ${CompilerFlagsRelWithDebInfo} -shared -o ${OutDLL} -Wl,${LibraryLinkerFlagsRelWithDebInfo}";
else
    BuildCommand="${Compiler} ${EntryPoint} ${CompilerFlagsRelease} -o ${OutBin} -Wl,${LinkerFlagsRelease}";
    BuildDLLCommand="${Compiler} ${DLLEntryPoint} ${CompilerFlagsRelease} -shared -o ${OutDLL} -Wl,${LibraryLinkerFlagsRelease}";
fi;

echo "Compiling DLL (command follows below)...";
echo "${BuildDLLCommand}";
echo "";

# Now build the dynamic link library
${BuildDLLCommand};

if [ $? -ne 0 ]; then
    echo -e "${RED}***************************************${NC}";
    echo -e "${RED}*      !!! An error occurred!!!       *${NC}";
    echo -e "${RED}***************************************${NC}";
    exit 1;
fi;


# On success, display confirmation message.
echo -e "${GREEN}***************************************${NC}";
echo -e "${GREEN}*    Build completed successfully!    *${NC}";
echo -e "${GREEN}***************************************${NC}";


EndTime=`date +%T`;
echo "Build script finished execution at ${EndTime}.";

exit 0;
